#define F_CPU 16000000UL
#include "copilot-c99-codegen/copilot.h"
#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>
#include <stdio.h>

extern bool button = false;

void show (uint8_t anode, uint8_t cathode)
{
	PORTD = anode;
	PORTC = cathode;
}

void clear ()
{
	PORTD = 0;
	PORTC = 255;
}

int main(void)
{
	DDRD = 255;
	DDRC = 255;
	PORTC = 255;

	for (;;)
	{
	    button = PINB & (1 << PB0);
		step();
	}
}
