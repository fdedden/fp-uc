module Main where

import Language.Copilot
import Copilot.Compile.C99

import qualified Prelude as P

-- Integer values corresponding bits of a port
a, b, c, d, e, f, g, dp :: Word8
g = 1
f = 2
a = 4
b = 8
d = 16
e = 32
c = 64
dp = 128

digit8 :: Char -> Word8
digit8 char = case char of
    '0' -> foldr (+) 0 [a,b,c,d,e,f]
    '1' -> foldr (+) 0 [b,c]
    '2' -> foldr (+) 0 [a,b,g,e,d]
    '3' -> foldr (+) 0 [a,b,c,d,g]
    '4' -> foldr (+) 0 [f,g,b,c]
    '5' -> foldr (+) 0 [a,f,g,c,d]
    '6' -> foldr (+) 0 [a,f,g,c,d]
    '7' -> foldr (+) 0 [a,b,c]
    '8' -> foldr (+) 0 [a,b,c,d,e,f,g]
    '9' -> foldr (+) 0 [a,b,c,d,f,g]
    'a' -> foldr (+) 0 [a,b,c,e,f,g]
    'b' -> foldr (+) 0 [f,g,c,d,e]
    'c' -> foldr (+) 0 [a,f,e,d]
    'd' -> foldr (+) 0 [b,c,d,e,g]
    'e' -> foldr (+) 0 [a,f,g,e,d]
    'f' -> foldr (+) 0 [a,f,g,e]
    '.' -> dp

-- At every tick, the complete display is updated
interval :: Stream Bool
interval = cycle [True,True,True,True,False,False,False,False]

cathodes :: Stream Word8
cathodes = [1,2,4,8] ++ cathodes

counter :: [Word16]
counter = counter' 0 where
    counter' :: Word16 -> [Word16]
    counter' n | n P.> 10 = []
               | otherwise = replicate 40 n P.++ (counter' (n+1))

-- Really simple way to translate 4 characters to 4 Word8's
-- If less than 4 characters are given, the display is right-aligned
display :: [Char] -> [Word8]
display (w:x:y:z:[]) = [digit8 w, digit8 x, digit8 y, digit8 z]
display (x:y:z:[]) = [0, digit8 x, digit8 y, digit8 z]
display (y:z:[]) = [0, 0, digit8 y, digit8 z]
display (z:[]) = [0, 0, 0, digit8 z]
display _ = undefined

anodes :: Stream Word8
anodes = cycle $ concatMap (\x -> display (show x)) counter

spec :: Spec
spec = do
    trigger "show" interval [arg anodes, arg cathodes]
    trigger "clear" (not interval) []

main = reify spec >>= compile defaultParams
