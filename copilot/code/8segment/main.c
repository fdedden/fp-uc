#define F_CPU 16000000UL

#include "copilot-c99-codegen/copilot.h"

#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>
#include <stdio.h>

void show (uint8_t anode, uint8_t cathode)
{
	PORTD = anode;
	PORTC = ~cathode;
}

void clear ()
{
	PORTD = 0;
	PORTC = 255;
}

int main(void)
{
	DDRD = 255;
	DDRC = 255;
	PORTC = 255;

	TCCR1B = (1 << CS12) | (1 << CS10);

	for (;;)
	{
		step();
	}
}
