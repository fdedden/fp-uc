#define F_CPU 16000000UL

#include "copilot-c99-codegen/copilot.h"

#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>
#include <stdio.h>

#define dbgout (*((volatile char *)0x20))
#define dbgin (*((volatile char *)0x22))

/*#define PRESCALE 1024UL Don't forget to set TCCR1B*/
#define PRESCALE 1UL

uint16_t t1;

void dprintf(const char *str) {
	const char *c;

	for (c = str; *c; ++c) {
		dbgout = *c;
	}
}

void setpin(uint8_t pin)
{
	PORTB |= (1 << pin);
}

void clearpin(uint8_t pin)
{
	PORTB &= ~(1 << pin);
}

void reset_t1(void)
{
	TCNT1 = 0;
	t1 = 0;
}

void a() {}
void b() {}
void c() {}

/* Calculate number of steps in the timer to create a delay */
uint16_t delay(uint16_t us)
{
	return ((us / F_CPU) * (us / PRESCALE) - 1); /* TODO: fix this */
}

int main(void)
{
	DDRB |= _BV(PB5) | _BV(PB5);

	/*TCCR1B |= (1<<CS12) | (1 << CS10);*/
	TCCR1B |= (1 << CS10);

	uint16_t _dbga = 0, _dbgb = 0;
	char str[10];

	while(1)
	{
		/*t1 = TCNT1;*/
		TCNT1 = 0;
		_dbga = TCNT1;
		step();

		_dbgb = TCNT1;

		sprintf(str, "%d %d %d\n", TCNT1, _dbga, _dbgb);
		dprintf(str);

		/*_delay_ms(500);*/
	}
	return 0;
}
