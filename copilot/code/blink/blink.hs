module Main where

import Language.Copilot
import Copilot.Compile.C99

import Prelude()

--fcpu = 1000000 :: Word32
--prescale = 1024 :: Word32

--delay us = (us `div` 1000000) * (us `div` 1024) - 1
--delayms ms = delay (ms * 1000)

delay :: Stream Word16 -> Stream Word16
delay us = externFun "delay" [arg us] Nothing
delayms ms = delay (ms * 1000)

t1 :: Stream Word16
t1 = extern "t1" Nothing

pin :: Stream Word8
pin = 5

spec :: Spec
spec = do
    trigger "reset_t1" (t1 > 2000) []
    trigger "clearpin" (t1 > 1000) [arg pin]
    --trigger "setpin" (t1 > 1000) [arg pin]
    --trigger "a" (t1 > 2300) []
    --trigger "b" (t1 > 100) []
    --trigger "c" (t1 > 200) []

main = reify spec >>= compile defaultParams
