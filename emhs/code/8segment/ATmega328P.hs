module ATmega328P where

import Language.Copilot as CP

{-
 - This file specifies the registers of the ATmega328P microcontroller.
 - All of these are specific to that microcontroller, but are easily adaptable
 - to different controllers of the same architecture.
-}

-- The pins on our ATmega
data Pin
    = PB0 | PB1 | PB2 | PB3 | PB4 | PB5 | PB6 | PB7
    | PC0 | PC1 | PC2 | PC3 | PC4 | PC5 | PC6
    | PD0 | PD1 | PD2 | PD3 | PD4 | PD5 | PD6 | PD7
    deriving (Show, Eq)

-- The ports on our ATmega
data Port = PORTB | PORTC | PORTD deriving (Show, Eq)
data PortIn = PINB | PINC | PIND deriving (Show, Eq)

-- Registers to store our data direction
data DDR = DDRB | DDRC | DDRD deriving (Show, Eq)

pins :: Port -> [Pin]
pins PORTB = [PB0, PB1, PB2, PB3, PB4, PB5, PB6, PB7]
pins PORTC = [PC0, PC1, PC2, PC3, PC4, PC5, PC6]
pins PORTD = [PD0, PD1, PD2, PD3, PD4, PD5, PD6, PD7]

ddr :: Port -> DDR
ddr PORTB = DDRB
ddr PORTC = DDRC
ddr PORTD = DDRD

-- Translate pin to bit value
-- Bit of a poor implementation, but it does the job
bitvalue :: Pin -> Word8
bitvalue pin = case pin of
    PB0 -> 1
    PB1 -> 2
    PB2 -> 4
    PB3 -> 8
    PB4 -> 16
    PB5 -> 32
    PB6 -> 64
    PB7 -> 128

    PC0 -> 1
    PC1 -> 2
    PC2 -> 4
    PC3 -> 8
    PC4 -> 16
    PC5 -> 32
    PC6 -> 64

    PD0 -> 1
    PD1 -> 2
    PD2 -> 4
    PD3 -> 8
    PD4 -> 16
    PD5 -> 32
    PD6 -> 64
    PD7 -> 128

maxvalue :: Port -> Word8
maxvalue PORTB = 255
maxvalue PORTC = 127
maxvalue PORTD = 255

portof :: Pin -> Port
portof pin = case pin of
    PB0 -> PORTB
    PB1 -> PORTB
    PB2 -> PORTB
    PB3 -> PORTB
    PB4 -> PORTB
    PB5 -> PORTB
    PB6 -> PORTB
    PB7 -> PORTB

    PC0 -> PORTC
    PC1 -> PORTC
    PC2 -> PORTC
    PC3 -> PORTC
    PC4 -> PORTC
    PC5 -> PORTC
    PC6 -> PORTC

    PD0 -> PORTD
    PD1 -> PORTD
    PD2 -> PORTD
    PD3 -> PORTD
    PD4 -> PORTD
    PD5 -> PORTD
    PD6 -> PORTD
    PD7 -> PORTD

inputof :: Port -> PortIn
inputof PORTB = PINB
inputof PORTC = PINC
inputof PORTD = PIND
