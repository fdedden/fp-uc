;; Ryan Suchocki
;; microscheme.org

(define led-pin (output 13))

(define (loop)
	(toggle led-pin)
	(pause 500))

(forever loop)
