/* Implements a multiplexed display consisting of multiple 8segment displays */

#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>

/* Pins corresponding to pins on a single display
 * This depends on the wiring of the display */
#define G  (1 << 0)
#define F  (1 << 1)
#define A  (1 << 2)
#define B  (1 << 3)
#define D  (1 << 4)
#define E  (1 << 5)
#define C  (1 << 6)
#define DP (1 << 7)


/* Ports corresponding to a character on a single display */
#define C0  (A | B | C | D | E | F)
#define C1  (B | C) /* Right alligned */
#define C2  (A | B | G | E | D)
#define C3  (A | B | C | D | G)
#define C4  (F | G | B | C)
#define C5  (A | F | G | C | D)
#define C6  (A | F | G | C | D | E)
#define C7  (A | B | C)
#define C8  (A | B | C | D | E | F | G)
#define C9  (A | B | C | D | F | G)
#define CA  (A | B | C | E | F | G)
#define CB  (F | G | C | D | E)
#define CC  (A | F | E | D)
#define CD  (B | C | D | E | G)
#define CE  (A | F | G | E | D)
#define CF  (A | F | G | E)
#define CDP DP

#define NR_DISP 4
#define DISP_INTERVAL (1 << 4) /* Number of timer ticks before interrupt */
#define DUTY_PHASE 4 /* How much interrupts before redrawing display */

#if NR_DISP > DUTY_PHASE
#error "NR_DISP > DUTY_PHASE"
#endif

/* Current display and text */
uint8_t cdisp = 0;
char disp[4] = "abcd";

/* Converts a char to pins */
uint8_t digit8 (char b)
{
	switch (b) {
		case '0': return C0;
		case '1': return C1;
		case '2': return C2;
		case '3': return C3;
		case '4': return C4;
		case '5': return C5;
		case '6': return C6;
		case '7': return C7;
		case '8': return C8;
		case '9': return C9;
		case 'a': return CA;
		case 'b': return CB;
		case 'c': return CC;
		case 'd': return CD;
		case 'e': return CE;
		case 'f': return CF;
		case '.': return CDP;
		case ' ': return 0;
		case '\0': return 0;
		default: return (G | DP); /* Indicating an error */
	}
}

/* Interrupt to update the display.
 * Interrupts needs to be called DUTY_PHASE times, before the first segment
 * display is updated again.
 * */
ISR (TIMER1_COMPA_vect)
{
	if (cdisp < NR_DISP) {
		PORTD = digit8(disp[cdisp]);
		PORTC = ~(1 << cdisp);
	}
	else {
		PORTD = 0;
		PORTC = 255;
	}

	++cdisp;
	if (cdisp == DUTY_PHASE) {
		cdisp = 0;
	}
}

int
main (void)
{
	DDRD = 255; /* Use all of PORTD as output */
	DDRC = 255;
	PORTC = 255; /* Disable all displays */

	/* Timer for display */
	OCR1A = DISP_INTERVAL;
	TCCR1B |= (1 << CS10) | (1 << CS12) | (1 << WGM12);
	TIMSK1 |= (1 << OCIE1A);

	sei();

	uint16_t n = 0;
	for (;;)
	{
		sprintf(disp, "%.4d", n);
		++n;
		if (n > pow(10,NR_DISP)-1) { /* > 9999 for 4 displays */
			n = 0;
		}
		_delay_ms(20);
	}
}
