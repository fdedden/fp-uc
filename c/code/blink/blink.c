#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

#define LED PB5

int
main (void)
{
	DDRB |= (1 << LED);

	while(1)
	{
		PORTB ^= (1 << LED);
		_delay_ms(500);
	}
}
