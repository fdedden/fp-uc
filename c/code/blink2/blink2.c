#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

#define LED1 5
#define LED2 4

#if 0
/* This does not work, we can't share two LED's on a single timer
 * when using interrupts. */
main (void)
{
	/* Set comparator value */
	OCR1A = F_CPU / (64 * 1) - 1; /* input freq / (prescale * target freq) - 1 */
	OCR1B = F_CPU / (64 * 5) - 1;

	DDRB |= (1 << LED1) | (1 << LED2);

	TCCR1B |= (1 << WGM12); /* Set 16bit timer to CTC mode */
	TCCR1B |= (1 << CS10) | (1 << CS11); /* Prescale of 64 */


	TIMSK1 |= (1 << OCIE1A) | (1 << OCIE1B); /* Enable specific interrupts */
	sei();

	/* Needed in order to keep the uproc running */
	for (;;) {
	}
}

ISR(TIMER1_COMPA_vect)
{
	PORTB ^= 1 << LED1;
}

ISR(TIMER1_COMPB_vect)
{
	PORTB ^= 1 << LED2;
}
#endif

/* This one works though */
#if 1

#define len(x)          (sizeof(x)/sizeof(x[0]))

#define PRESCALE        64
#define PRESCALE_BITS   (1 << CS10 | 1 << CS11)

#define DELAY1 500
#define DELAY2 100

#define FREQ1 (1000/DELAY1)
#define FREQ2 (1000/DELAY2)

typedef struct {
	int duration;
	int value;
} step;

typedef struct {
	int pin;
	int count;
	step *currstep;
	step *steps;
} stream;

main (void)
{
	DDRB |= (1 << LED1) | (1 << LED2);

	TCCR1B |= PRESCALE_BITS;

	int t1 = (F_CPU / (PRESCALE * FREQ1)) - 1; /* input freq / (prescale * target freq) - 1 */
	int t2 = (F_CPU / (PRESCALE * FREQ2)) - 1;

	step steps5[] = {
		{.duration = t1, .value = 1},
		{.duration = t1, .value = 0}
	};
	step steps4[] = {
		{.duration = t2, .value = 1},
		{.duration = t2, .value = 0}
	};

	stream streams[] = {
		{LED1, len(steps5), steps5, steps5},
		{LED2, len(steps4), steps4, steps4}
	};
	
	for (;;) {
		int i = 0;
		int updated = 0;
		for (i = 0; i < len(streams); ++i) {
			stream *s = &streams[i];
			if (TCNT1 >= s->currstep->duration) {
				int curr = TCNT1;
				TCNT1 = 0;
				s->currstep++;
				if (s->currstep == (s->steps + s->count)) {
					s->currstep = s->steps;
				}
				PORTB ^= (-(s->currstep->value) ^ PORTB) & (1 << s->pin);
				if (updated == 0) {
					int j = 0;
					for (j = 0; j < len(streams); ++j) {
						if (j != i) {
							streams[j].currstep->duration -= curr;
						}
					}
					updated = 1;
				}
			}
		}
	}
}
#endif
